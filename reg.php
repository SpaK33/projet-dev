<?php
// Connexion à la base de données
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cuisine";

$conn = mysqli_connect($servername, $username, $password, $dbname);

// Vérification de la connexion
if (!$conn) {
    die("Connexion échouée: " . mysqli_connect_error());
}

// Récupération des données du formulaire
$username = $_POST['username'];
$email = $_POST['email'];
$password = $_POST['password'];
$nom = $_POST['nom'];
$prenom = $_POST['prenom'];

// Cryptage du mot de passe
$password = password_hash($password, PASSWORD_DEFAULT);

// Insertion des données dans la base de données
$sql = "INSERT INTO utilisateurs (username, email, password, nom, prenom)
VALUES ('$username', '$email', '$password', '$nom', '$prenom')";

if (mysqli_query($conn, $sql)) {
    echo "Inscription réussie";
} else {
    echo "Erreur: " . $sql . "<br>" . mysqli_error($conn);
}

mysqli_close($conn);