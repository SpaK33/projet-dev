<!DOCTYPE html>
<html>
<head>
	<title>Ajouter une recette</title>
</head>
<body>
	<h1>Ajouter une recette</h1>
	<form method="post" enctype="multipart/form-data">
		<label for="titre">Titre :</label>
		<input type="text" name="titre" required>
		<br><br>
		<label for="image">Image :</label>
		<input type="file" name="image" accept="image/*" required>
		<br><br>
		<label for="ingredients">Ingrédients :</label>
		<textarea name="ingredients" required></textarea>
		<br><br>
		<label for="instructions">Instructions :</label>
		<textarea name="instructions" required></textarea>
		<br><br>
		<input type="submit" name="ajouter" value="Ajouter">
	</form>

	<?php
	// Vérifier si le formulaire a été soumis
	if (isset($_POST['ajouter'])) {

		// Récupérer les données du formulaire
		$titre = $_POST['titre'];
		$image = $_FILES['image']['name'];
		$ingredients = $_POST['ingredients'];
		$instructions = $_POST['instructions'];

		// Vérifier si une image a été sélectionnée
		if ($image) {
			// Déplacer l'image téléchargée dans le dossier "images"
			move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $image);
		}

		// Ajouter la recette à la base de données (ici, nous supposons que nous avons une base de données MySQL)
		$bdd = new PDO('mysql:host=localhost;dbname=cuisine;charset=utf8', 'root', '');
		$req = $bdd->prepare('INSERT INTO recettes (titre, image, ingredients, instructions) VALUES (?, ?, ?, ?)');
		$req->execute(array($titre, $image, $ingredients, $instructions));

		// Afficher un message de confirmation
		echo "La recette a été ajoutée avec succès !";
	}
	?>
</body>
</html>
