<?php
session_start();

// Vérifier si l'utilisateur est déjà connecté
if (isset($_SESSION['user_id'])) {
    header('Location: connexion.php');
    exit;
}

// Vérifier si le formulaire de connexion a été soumis
if (isset($_POST['submit'])) {
    // Vérifier si les champs d'email et de mot de passe ont été remplis
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        // Connectez-vous à la base de données et récupérez les informations de l'utilisateur
        $pdo = new PDO('mysql:host=localhost;dbname=cuisine;charset=utf8', 'root', '');
        $stmt = $pdo->prepare('SELECT * FROM users WHERE email = ?');
        $stmt->execute([$_POST['email']]);
        $user = $stmt->fetch();

        // Vérifier si l'utilisateur a été trouvé et que le mot de passe est correct
        if ($user && password_verify($_POST['password'], $user['password'])) {
            // Stocker l'identifiant de l'utilisateur dans une variable de session
            $_SESSION['user_id'] = $user['id'];

            // Rediriger l'utilisateur vers la page de tableau de bord
            header('Location: index.php');
            exit;
        } else {
            $error = 'Adresse e-mail ou mot de passe incorrect';
        }
    } else {
        $error = 'Veuillez remplir tous les champs';
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Page de connexion</title>
</head>
<body>
    <h1>Page de connexion</h1>

    <?php if (isset($error)): ?>
        <div style="color: red;"><?php echo $error; ?></div>
    <?php endif; ?>

    <form action="index.php" method="post">
        <label>
            Adresse e-mail:
            <input type="email" name="email" required>
        </label>
        <br>
        <label>
            Mot de passe:
            <input type="password" name="password" required>
        </label>
        <br>
        <button type="submit" name="submit">Se connecter</button>
    </form>
</body>
</html>
