<?php
include 'inc/header.php'
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Recettes de cuisine</title>
	<meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
	<h1>Recettes de cuisine</h1>

	<?php
	// Connexion à la base de données
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "cuisine";

	$conn = mysqli_connect($servername, $username, $password, $dbname);

	// Vérification de la connexion
	if (!$conn) {
	    die("Connexion échouée: " . mysqli_connect_error());
	}

	// Récupération des recettes
	$sql = "SELECT * FROM recettes";
	$result = mysqli_query($conn, $sql); 

	if (mysqli_num_rows($result) > 0) {
	    // Affichage de toutes les recettes
	    while($row = mysqli_fetch_assoc($result)) {
	        echo "<h2>" . $row["titre"] . "</h2>";
	        echo "<img src='" . $row["image"] . "'><br>";
	        echo "<b>Ingrédients:</b><br>" . $row["ingredients"] . "<br><br>";
	        echo "<b>Instructions:</b><br>" . $row["instructions"] . "<br><br>";
	    }
	} else {
	    echo "Aucune recette trouvée";
	}

	mysqli_close($conn);
	?>

	<a href="ajout_recette.php">Ajouter une recette</a>
</body>
</html>