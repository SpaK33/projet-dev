<!DOCTYPE html>
<html>
<head>
	<title>Mon site web</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<header>
		<nav>
			<ul>
				<li><a href="index.php">Accueil</a></li>
				<li><a href="#">À propos</a></li>
				<li><a href="#">Contact</a></li>
				<?php if (isset($_SESSION['user_id'])) { ?>
					<li><a href="profil.php">Mon compte</a></li>
					<li><a href="#">Déconnexion</a></li>
				<?php } else { ?>
					<li><a href="connexion.php">Connexion</a></li>
					<li><a href="inscription.php">Inscription</a></li>
				<?php } ?>
			</ul>
		</nav>
	</header>
	<main>
		<!-- Le contenu de la page -->
	</main>
</body>
</html>