<?php
session_start(); // On démarre la session

// Si l'utilisateur est déjà déconnecté, on le redirige vers la page d'accueil
if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    exit();
}

// Si l'utilisateur clique sur le bouton "Se déconnecter"
if (isset($_POST['logout'])) {
    // On supprime toutes les variables de session
    session_unset();

    // On détruit la session
    session_destroy();

    // On redirige l'utilisateur vers la page d'accueil
    header("Location: index.php");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Se déconnecter</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <h1>Se déconnecter</h1>
    <p>Êtes-vous sûr(e) de vouloir vous déconnecter ?</p>
    <form method="post">
        <button type="submit" name="logout">Se déconnecter</button>
    </form>
</body>
</html>
